package main;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import logika.Hra;
import logika.IHra;
import uiText.TextoveRozhrani;

public class AdventuraZaklad extends Application {

    private BorderPane borderPane;
    private TextArea centerTextArea ;
    private IHra hra ;
    private FlowPane dolniFlowPane ;
    private Label zadejPrikazLabel ;
    private TextField prikazTextField ;


    public static void main(String[] args) {
        if (args.length == 0) {
            launch(args);
        } else {
            if (args[0].equals("-text")) {
                IHra hra = new Hra();
                TextoveRozhrani ui = new TextoveRozhrani(hra);
                ui.hraj();
                System.exit(0);
            } else {
                System.out.println("Neplatny parametr");
            }
        }

    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent e) {
                Platform.exit();
                System.exit(0);
            }
        });

        borderPane = new BorderPane();
        hra = new Hra();
        nastavTextArea();
        borderPane.setCenter(centerTextArea);
        nastavDolniPanel();
        borderPane.setBottom(dolniFlowPane);

        Scene scene = new Scene(borderPane, 600, 450);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Adventura");
        prikazTextField.requestFocus();
        primaryStage.show();

    }

    private void nastavTextArea() {
        centerTextArea = new TextArea();
        centerTextArea.setText(hra.vratUvitani());
        centerTextArea.setEditable(false);
    }

    private void nastavDolniPanel() {
        dolniFlowPane = new FlowPane();
        zadejPrikazLabel = new Label("Zadej prikaz: ");
        zadejPrikazLabel.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        prikazTextField = new TextField();
        prikazTextField.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                String radek = prikazTextField.getText();
                centerTextArea.appendText("\n" + radek + "\n");
                String text = hra.zpracujPrikaz(radek);
                centerTextArea.appendText("\n" + text + "\n");
                prikazTextField.setText("");
                if (hra.konecHry()) {
                    prikazTextField.setEditable(false);
                }

            }

        });

        dolniFlowPane.setAlignment(Pos.CENTER);
        dolniFlowPane.getChildren().addAll(zadejPrikazLabel, prikazTextField);
    }
}
